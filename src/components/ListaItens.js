
import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';

import Itens from './Itens';
import axios from 'axios';

export default class ListaItens extends Component {

    constructor(props){
        super(props);

        this.state = { listaItens: [] }
    }

    componentWillMount(){
        axios.get('http://www.faus.com.br/recursos/c/dmairr/api/itens.html')
        .then(response => {
            console.log(response)
            this.setState({ listaItens: response.data })
        })
        .catch(() => {
            console.log('Erro')
        });
    }

    render() {
        return (
            <ScrollView style={{ paddingTop: 40, backgroundColor: '#f2f2f2' }}>
                { this.state.listaItens.map((item) => 
                    <Itens item={ item } key={ item.titulo }>{ item.titulo }</Itens>
                ) }
            </ScrollView>
        );
    }
}
