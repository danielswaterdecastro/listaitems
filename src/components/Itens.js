
import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';

var estilo = StyleSheet.create({
    item: {
        borderWidth: 0.5,
        borderColor: '#afafaf',
        backgroundColor: 'white',
        padding: 10,
        margin: 10,
        flexDirection: 'row',
    },
    imagem: {
        width: 100,
        height: 100,
    },
    foto: {
        width: 102,
        height: 102
    },
    detalhe: {
        marginLeft: 15,
        //a propriedade flex aqui, e para o texto se enquadradar na view
        flex: 1,
    },
    titulo: {
        color: 'blue',
        fontSize: 15,
        marginBottom: 5
    }
})

export default class Itens extends Component {
    
    render() {
        return (
            <View style={ estilo.item }>
                <View style={ estilo.foto }>
                    <Image style={ estilo.imagem } source={ {uri: this.props.item.foto} }></Image>
                </View>
                <View style={ estilo.detalhe }>
                    <Text style={ estilo.titulo }>{ this.props.item.titulo }</Text>
                    <Text>R$ { this.props.item.valor }</Text>
                    <Text>Local: { this.props.item.local_anuncio }</Text>
                    <Text>Data: { this.props.item.data_publicacao }</Text>
                </View>
            </View>
        );
    }
}
